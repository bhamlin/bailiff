use std::os::unix::net::UnixListener;

#[allow(unused)]
pub struct BailiffSocket {
    pub socket_file: UnixListener,
}
