use bailiff_common::error::LibBailiffError;
use error_stack::{Context, Report};
use std::fmt::Display;

#[derive(Debug)]
pub enum BailiffError {
    SocketFailure,
    UnhandledError,
}

impl Context for BailiffError {}
impl Display for BailiffError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::SocketFailure => "Socket failure",
            Self::UnhandledError => "Unhandled error",
        };

        write!(f, "{message}")
    }
}

impl From<LibBailiffError> for BailiffError {
    fn from(value: LibBailiffError) -> Self {
        #[allow(clippy::match_single_binding)]
        #[allow(unreachable_patterns)]
        match value {
            _ => Self::UnhandledError,
        }
    }
}

impl BailiffError {
    pub fn from_lib<T>(
        result: Result<T, Report<LibBailiffError>>,
    ) -> Result<T, Report<BailiffError>> {
        match result {
            Ok(value) => Ok(value),
            Err(error) => match error.current_context() {
                LibBailiffError::ByteParseError => {
                    Err(error.change_context(BailiffError::SocketFailure))
                }
                LibBailiffError::SocketFailure => {
                    Err(error.change_context(BailiffError::SocketFailure))
                }
            },
        }
    }
}
