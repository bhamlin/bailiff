mod error;
mod socket;

use bailiff_common::socket::unix::UnixSocketListener;
use error::BailiffError;
use error_stack::{Result, ResultExt};
#[allow(unused)]
use log::{debug, info};

#[tokio::main]
async fn main() -> Result<(), BailiffError> {
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or(log::Level::Debug.as_str()),
    )
    .init();

    let socket_path = "/tmp/bailiff.sock";

    // let unix_listener = create_listener(socket_path)?;
    let mut listener = BailiffError::from_lib(UnixSocketListener::new(socket_path))?;

    // put the server logic in a loop to accept several connections
    info!("Listening on {socket_path}");
    loop {
        listener
            .listen()
            .await
            .change_context_lazy(|| BailiffError::SocketFailure)
            .attach_printable_lazy(|| "Error on socket")?
    }
}
