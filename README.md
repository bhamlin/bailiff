# Bailiff

Rust based ssh agent and daemon for remote authentication management.

Resources:

- [OpenSSH docs](https://github.com/openssh/openssh-portable/blob/master/PROTOCOL.agent)
- [SSH Agent RFC](https://datatracker.ietf.org/doc/html/draft-miller-ssh-agent#name-protocol-messages)
