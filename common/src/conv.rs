pub fn hex_string(data: &[u8]) -> String {
    let mut output = String::with_capacity(data.len() * 2);

    for &value in data {
        output.push_str(&format!("{value:02x}"));
    }

    output
}

pub fn asc_string(data: &[u8]) -> String {
    let mut output = String::with_capacity(data.len());

    for &value in data {
        output.push(' ');
        output.push(match value {
            0x20..=0x7e => value as char,
            _ => '.',
        });
    }

    output
}

pub fn text_string(data: &[u8]) -> String {
    let mut output = String::with_capacity(data.len());

    for &value in data {
        output.push(match value {
            0x20..=0x7e => value as char,
            _ => ' ',
        });
    }

    output
}
