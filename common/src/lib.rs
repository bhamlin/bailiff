use std::{fmt::Display, rc::Rc};

pub mod conv;
pub mod error;
pub mod message;
pub mod socket;

#[derive(PartialEq)]
pub enum SshClientIds {
    SshAgentcRequestIdentities,
    SshAgentcSignRequest,
    SshAgentcAddIdentity,
    SshAgentcRemoveIdentity,
    SshAgentcRemoveAllIdentities,
    SshAgentcAddSmartcardKey,
    SshAgentcRemoveSmartcardKey,
    SshAgentcLock,
    SshAgentcUnlock,
    SshAgentcAddIdConstrained,
    SshAgentcAddSmartcardKeyConstrained,
    SshAgentcExtension,
    Unknown,
}

impl From<u8> for SshClientIds {
    fn from(value: u8) -> Self {
        match value {
            0x0b => Self::SshAgentcRequestIdentities,
            0x0d => Self::SshAgentcSignRequest,
            0x11 => Self::SshAgentcAddIdentity,
            0x12 => Self::SshAgentcRemoveIdentity,
            0x13 => Self::SshAgentcRemoveAllIdentities,
            0x14 => Self::SshAgentcAddSmartcardKey,
            0x15 => Self::SshAgentcRemoveSmartcardKey,
            0x16 => Self::SshAgentcLock,
            0x17 => Self::SshAgentcUnlock,
            0x19 => Self::SshAgentcAddIdConstrained,
            0x1a => Self::SshAgentcAddSmartcardKeyConstrained,
            0x1b => Self::SshAgentcExtension,
            _ => Self::Unknown,
        }
    }
}

impl Display for SshClientIds {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = self.c_const();

        write!(f, "{message}")
    }
}

impl SshClientIds {
    pub fn c_const(&self) -> Rc<str> {
        match self {
            Self::SshAgentcRequestIdentities => "SSH_AGENTC_REQUEST_IDENTITIES",
            Self::SshAgentcSignRequest => "SSH_AGENTC_SIGN_REQUEST",
            Self::SshAgentcAddIdentity => "SSH_AGENTC_ADD_IDENTITY",
            Self::SshAgentcRemoveIdentity => "SSH_AGENTC_REMOVE_IDENTITY",
            Self::SshAgentcRemoveAllIdentities => "SSH_AGENTC_REMOVE_ALL_IDENTITIES",
            Self::SshAgentcAddSmartcardKey => "SSH_AGENTC_ADD_SMARTCARD_KEY",
            Self::SshAgentcRemoveSmartcardKey => "SSH_AGENTC_REMOVE_SMARTCARD_KEY",
            Self::SshAgentcLock => "SSH_AGENTC_LOCK",
            Self::SshAgentcUnlock => "SSH_AGENTC_UNLOCK",
            Self::SshAgentcAddIdConstrained => "SSH_AGENTC_ADD_ID_CONSTRAINED",
            Self::SshAgentcAddSmartcardKeyConstrained => "SSH_AGENTC_ADD_SMARTCARD_KEY_CONSTRAINED",
            Self::SshAgentcExtension => "SSH_AGENTC_EXTENSION",
            Self::Unknown => "Unknown",
        }
        .into()
    }

    pub fn f_const(&self) -> Rc<str> {
        match self {
            Self::SshAgentcRequestIdentities => "SshAgentcRequestIdentities",
            Self::SshAgentcSignRequest => "SshAgentcSignRequest",
            Self::SshAgentcAddIdentity => "SshAgentcAddIdentity",
            Self::SshAgentcRemoveIdentity => "SshAgentcRemoveIdentity",
            Self::SshAgentcRemoveAllIdentities => "SshAgentcRemoveAllIdentities",
            Self::SshAgentcAddSmartcardKey => "SshAgentcAddSmartcardKey",
            Self::SshAgentcRemoveSmartcardKey => "SshAgentcRemoveSmartcardKey",
            Self::SshAgentcLock => "SshAgentcLock",
            Self::SshAgentcUnlock => "SshAgentcUnlock",
            Self::SshAgentcAddIdConstrained => "SshAgentcAddIdConstrained",
            Self::SshAgentcAddSmartcardKeyConstrained => "SshAgentcAddSmartcardKeyConstrained",
            Self::SshAgentcExtension => "SshAgentcExtension",
            Self::Unknown => "Unknown",
        }
        .into()
    }
}

pub enum SshAgentIds {
    SshAgentFailure,
    SshAgentSuccess,
    SshAgentIdentitiesAnswer,
    SshAgentSignResponse,
    SshAgentExtensionFailure,
}

pub enum SshConstraintIds {
    SshAgentConstrainLifetime,
    SshAgentConstrainConfirm,
    SshAgentConstrainExtension,
}

pub enum SshSignatureIds {
    SshAgentRsaSha2_256,
    SshAgentRsaSha2_512,
}
