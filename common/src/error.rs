use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum LibBailiffError {
    ByteParseError,
    SocketFailure,
}

impl Context for LibBailiffError {}
impl Display for LibBailiffError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::ByteParseError => "Byte parsing error",
            Self::SocketFailure => "Socket failure",
        };

        write!(f, "{message}")
    }
}
