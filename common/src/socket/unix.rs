// use super::SshSocketListener;
use crate::{
    conv::{asc_string, hex_string},
    error::LibBailiffError,
    message::ByteStream,
    SshClientIds,
};
use error_stack::{Result, ResultExt};
use log::debug;
use std::{io::Read, os::unix::net::UnixListener};

pub struct UnixSocketListener {
    buffer: [u8; 1024],
    // path: String,
    listener: UnixListener,
    // handlers: Vec<Box<dyn SshMessageHandler + 'static>>,
}

// impl SshSocketListener for UnixSocketListener {
impl UnixSocketListener {
    pub fn new(spec: &str) -> Result<Self, LibBailiffError> {
        if std::fs::metadata(spec).is_ok() {
            debug!("Cleaning old socket...");
            std::fs::remove_file(spec)
                .change_context(LibBailiffError::SocketFailure)
                .attach_printable_lazy(|| {
                    format!("could not delete previous socket at {:?}", spec)
                })?;
        };

        let unix_listener = UnixListener::bind(spec)
            .change_context(LibBailiffError::SocketFailure)
            .attach_printable("Could not create the unix socket")?;

        Ok(UnixSocketListener {
            buffer: [0; 1024],
            // path: spec.to_string(),
            listener: unix_listener,
            // handlers: Default::default(),
        })
    }

    pub async fn listen(&mut self) -> Result<(), LibBailiffError> {
        let (mut unix_stream, _) = self
            .listener
            .accept()
            .change_context_lazy(|| LibBailiffError::SocketFailure)
            .attach_printable_lazy(|| "Error while awaiting input")?;
        // debug!("Connect from: {socket_address:?}");
        let got_bytes = unix_stream
            .read(&mut self.buffer)
            .change_context_lazy(|| LibBailiffError::SocketFailure)
            .attach_printable_lazy(|| "Failed at reading the unix stream")?;
        debug!("Got {got_bytes} bytes");
        UnixSocketListener::handle_message(ByteStream::from(&self.buffer[..got_bytes]));

        Ok(())
    }

    fn handle_message(mut message: ByteStream) {
        debug!("Pull size u32");
        let m_len = message.get_u32_be() - 1;
        debug!("  {m_len} bytes + type byte");
        debug!("Pull type");
        let m_type = message.get_u8();
        debug!("  {m_type:02x}: {}", SshClientIds::from(m_type));
        // I should be expecting a 1b here?
        if SshClientIds::from(m_type) == SshClientIds::SshAgentcExtension {
            UnixSocketListener::handle_ssh_thingy(&mut message);
        }
    }

    fn handle_ssh_thingy(message: &mut ByteStream) {
        debug!("{}", message.as_hex_string());
        debug!("{}", message.as_asc_string());

        if !message.is_empty() {
            while !message.is_empty() {
                let m_len = message.get_u32_be() as usize;
                let m_type = message.get_u8();
                let m_body = message.get_stream(m_len - 1);
                debug!("-----------");
                debug!("size: {m_len}");
                debug!("type: {m_type:02x} ({})", SshClientIds::from(m_type));
                debug!("body:");
                debug!("  [{}]", hex_string(&m_body));
                debug!("  [{}]", asc_string(&m_body));
            }
            debug!("-----------");
        }
    }
}
