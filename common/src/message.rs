use crate::{
    conv::{hex_string, text_string},
    error::LibBailiffError,
};
use error_stack::{Result, ResultExt};
#[allow(unused)]
use log::{debug, info};
use std::rc::Rc;

pub enum SshMessage {}

impl SshMessage {
    pub fn parse_packet(data: &mut ByteStream) -> Result<(), LibBailiffError> {
        // let mut length: [u8; 4] = [0, 0, 0, 0];
        // length[..4].copy_from_slice(&data[..4]);
        // let length = u32::from_be_bytes(length) as usize;
        let length = data.get_u32_be() as usize;
        let message_len = data.len();

        if message_len != (length + 4) {
            return Err(LibBailiffError::ByteParseError).attach_printable(format!(
                "Expecting {} bytes, got {} bytes",
                length,
                message_len - 4
            ));
        } else {
            debug!("Got {length} bytes");
        }

        Ok(())
    }
}

pub struct ByteStream {
    data: Rc<[u8]>,
    cursor: usize,
    length: usize,
}

impl ByteStream {
    pub fn from(stream: &[u8]) -> ByteStream {
        let length = stream.len();
        let cursor = 0;
        let data = Rc::from(stream);

        ByteStream {
            data,
            cursor,
            length,
        }
    }

    pub fn get_u8(&mut self) -> u8 {
        self.get_one()
    }

    pub fn get_u32_be(&mut self) -> u32 {
        u32::from_be_bytes(self.get_four())
    }

    pub fn get_stream(&mut self, len: usize) -> Vec<u8> {
        let mut result = Vec::default();
        let i = self.cursor;
        result.extend_from_slice(&self.data[i..i + len]);
        self.cursor += len;
        result
    }

    fn get_one(&mut self) -> u8 {
        let result = self.data[self.cursor];
        self.cursor += 1;
        result
    }

    fn get_four(&mut self) -> [u8; 4] {
        let mut result = [0; 4];
        let i = self.cursor;
        result.copy_from_slice(&self.data[i..i + 4]);
        self.cursor += 4;
        result
    }

    pub fn is_empty(&self) -> bool {
        self.cursor == self.length
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn as_asc_string(&self) -> String {
        text_string(&self.data[self.cursor..])
    }

    pub fn as_hex_string(&self) -> String {
        hex_string(&self.data[self.cursor..])
    }
}

pub enum SshMessageHandlerResult {
    Pass,
    Drop,
}

pub trait SshMessageHandler: Sized {
    fn handle_message(
        &self,
        message: ByteStream,
    ) -> Result<SshMessageHandlerResult, LibBailiffError>;
}
